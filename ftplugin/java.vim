setl textwidth=100

inoreabbrev public public
inoreabbrev upblic public
inoreabbrev pbulic public
inoreabbrev pbulci public
inoreabbrev pulbic public
inoreabbrev pubilc public
inoreabbrev publci public

inoreabbrev rpivate private
inoreabbrev pirvate private
inoreabbrev prviate private
inoreabbrev priavte private
inoreabbrev privtae private
inoreabbrev privaet private
inoreabbrev pirvaet private
inoreabbrev priavet private
inoreabbrev piravet private

inoreabbrev tsatic static
inoreabbrev sattic static
inoreabbrev sttaic static
inoreabbrev staitc static
inoreabbrev statci static

inoreabbrev ovid void
inoreabbrev viod void
inoreabbrev vodi void
