local vim = assert(vim)
local augroup = vim.api.nvim_create_augroup   -- Create/get autocommand group
local autocmd = vim.api.nvim_create_autocmd   -- Create autocommand

M = {}

local function get_buffers(options)
    local buffers = {}
    local len = 0
    local options_listed = options.listed
    local vim_fn = vim.fn
    local buflisted = vim_fn.buflisted

    for buffer = 1, vim_fn.bufnr('$') do
        if not options_listed or buflisted(buffer) ~= 1 then
            len = len + 1
            buffers[len] = buffer
        end
    end

    return buffers
end

function M.goto_terminal()
  for _, buf in pairs(vim.fn.tabpagebuflist()) do
    if string.match(vim.fn.bufname(buf), "^term://.*") then
      local win = vim.fn.bufwinnr(buf)
      vim.api.nvim_exec(win .. "wincmd w", 0)
      return
    end
  end
end

local function contains(needle, haystack)
  for _, h in pairs(haystack) do
    if h == needle then
      return true
    end
  end
  return false
end

local function navigate_to(buffer)
  local wid = vim.fn.win_findbuf(buffer)
  if #wid > 0 then
    vim.fn.win_gotoid(wid[1])
  end
end

function M.goto_lastwin()
  if M.lastwin then
    vim.fn.win_gotoid(M.lastwin)
  end
end

function M.goto_initvim()
  local buffers = get_buffers({})
  for _, buf in pairs(buffers) do
    if string.match(vim.fn.bufname(buf), "nvim/init.vim$") then
      navigate_to(buf)
      return
    end
  end

  vim.api.nvim_exec("tabedit ~/.config/nvim/init.vim", 0)
end

augroup('WindowNav', { clear = true })
autocmd('WinLeave', {
  group = 'WindowNav',
  callback = function ()
    M.lastwin = vim.fn.win_getid()
  end
})

return M
