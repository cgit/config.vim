local vim = assert(vim)

-- LSP clients attached to buffer
local clients_lsp = function()
  local bufnr = vim.api.nvim_get_current_buf()

  local clients = vim.lsp.buf_get_clients(bufnr)
  if next(clients) == nil then
    return ''
  end

  local c = {}
  for _, client in pairs(clients) do
    table.insert(c, client.name)
  end
  return '\u{f085}  ' .. table.concat(c, ',')
end

local config = {
  options = {
    theme = 'meltdown',
    component_separators = '', -- '┃'
    section_separators = {left = '', right = ''},
  },
  sections = {
    lualine_a = { 'mode' },
    lualine_b = { 'branch', 'diff', 'diagnostics' },
    lualine_c = { 'filename' },

    -- right
    lualine_x = {
    --   { clients_lsp, color = { fg = '#80a0ff' } },
    --   { 'encoding', color = { fg = '#80a0ff' } },
    --   { 'fileformat', color = { fg = '#ffb700' } },
    --   { 'filetype', color = { fg = '#80a0ff' } },
    },
    lualine_y = {
      { clients_lsp, color = { fg = '#80a0ff' } },
      { 'encoding', color = { fg = '#80a0ff' } },
      { 'fileformat', color = { fg = '#ffb700' } },
      { 'filetype', color = { fg = '#80a0ff' } },
      'progress' },
    lualine_z = { 'location' }
  },
}

require("lualine").setup(config)
