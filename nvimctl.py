#!/usr/bin/env python3

import os
import sys
import neovim

try:
    nvim_socket = os.environ["NVIM"]
except KeyError:
    sys.stderr.write("Not inside Neovim terminal")
    sys.exit(1)

nvim = neovim.attach('socket', path=nvim_socket)


class GetReg:

    def name(self):
        return "getreg"

    def usage(self):
        return "[register]"

    def descr(self):
        return "Prints the contents of a Neovim register to stdout."

    def run(self, args):
        register = args[0]
        print(nvim.funcs.getreg(register))


class PutReg:

    def name(self):
        return "setreg"

    def usage(self):
        return "<register> [content]"

    def descr(self):
        return "Puts [content] into <register>. If no content is " + \
                "provided, it is read from stdin"

    def run(self, args):
        register = args[0]

        if len(args) > 1:
            lines = " ".join(args[1:])
        else:
            lines = sys.stdin.read()

        lines = lines.splitlines()

        if len(lines) == 1:
            lines = lines[0]

        nvim.funcs.setreg(register, lines)


class Feedkeys:

    def name(self):
        return "feedkeys"

    def usage(self):
        return "[args]"

    def descr(self):
        return "Feedkeys from args to neovim. If args is empty, stdin is used."

    def run(self, args):
        if len(args) < 1:
            keys = sys.stdin.read()
        else:
            keys = "".join(args)

        nvim.funcs.feedkeys(keys)


commands = [GetReg(), PutReg(), Feedkeys()]


def print_help():
    sys.stderr.write("USAGE %s <command> [args...]\n" % sys.argv[0])
    sys.stderr.write("commands:\n")
    for c in commands:
        sys.stderr.write("\t%s: %s\n" % (c.name(), c.descr()))


if len(sys.argv) < 2:
    print_help()
    sys.exit(1)

cmd = sys.argv[1]
args = sys.argv[2:]
for c in commands:
    if cmd == c.name():
        sys.exit(c.run(args) or 0)

sys.stderr.write("Unknown command %s\n" % cmd)
print_help()
