#!/bin/bash

set -e

plugged_dir="$HOME/.local/share/nvim/plugged"
mkdir -p "$plugged_dir"
cd "$plugged_dir"

git clone git@git.josher.dev:config.vim.git config.vim

mkdir -p "$HOME/.config/"
cd "$HOME/.config/"

ln -sf "$plugged_dir/config.vim" nvim

curl -fLo "$HOME/.local/share/nvim/site/autoload/plug.vim" --create-dirs \
       https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim

ln -sf "$HOME/.config/nvim/run_vim.py" "$HOME/.local/bin/vim"
ln -sf "$HOME/.config/nvim/nvimctl.py" "$HOME/.local/bin/nvimctl"
