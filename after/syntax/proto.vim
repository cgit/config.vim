" syn clear pbType
" syn clear pbRepeat
" syn clear pbMessage
" syn clear pbTypedef

syn keyword pbEnumTypedef enum nextgroup=pbEnumTypeName skipwhite skipempty

syn keyword pbRepeat required optional repeated nextgroup=protoFieldType skipempty skipwhite

syn cluster top contains=
      \pbRepeat,
      \protoComment,
      \protoInt,
      \protoFloat,
      \protoString,
      \pbEnumTypedef,
      \pbMessage2
      \protoSyntax,
      \protoStructure,
      \protoRepeat,
      \protoDefault,
      \protoExtend,
      \protoRPC,
      \pbAnnotation,
      \pbOneofKeyword,
      \pbOptionKeyword,
      \pbMessageKeyword

syn cluster oneofContains contains=protoFieldType,pbOptionKeyword,protoComment,pbAnnotation

syn keyword pbOneofKeyword oneof nextgroup=pbOneofName skipwhite
syn match pbOneofName +\<\w\+\>+ nextgroup=pbOneofBody contained skipwhite
syn region pbOneofBody start=+{+ end=+}+ contains=@oneofContains contained

syn match protoFieldType "\<\([A-Za-z0-9_]\+\|\_s*\.\_s*[A-Za-z0-9_]\+\)\+\>" nextgroup=protoFieldName skipempty skipwhite
syn match protoFieldName "\<\w\+\>" nextgroup=protoFieldEquals contained skipwhite
syn match protoFieldEquals +=+ contained nextgroup=protoFieldNumber,protoFieldNumberBad contained skipwhite
syn match protoFieldNumber +[1-9][0-9]*+ contained
syn match protoFieldNumberBad +0[0-9]*+ contained

hi! link protoFieldNumber Number
hi! link protoFieldEquals Operator
hi! link protoFieldNumberBad Error

syn match pbEnumTypeName "\<\w\+\>" nextgroup=enumBody skipempty skipwhite contained
syn region enumBody start=+{+ end=+}+ contains=enumValue,protoComment,protoInt,pbAnnotation fold contained
syn match enumValue "\zs\w\+\ze\s*=" contained

syn keyword pbExtendKeyword extend nextgroup=pbMessageQualifiedName skipwhite
syn match pbMessageQualifiedName "\<[a-zA-Z0-9_.]\+\>" nextgroup=pbMessageBody skipempty skipwhite

hi! link pbExtendKeyword protoExtend
hi! link pbMessageQualifiedName pbMessage2

syn keyword pbMessageKeyword message nextgroup=pbMessage2 skipwhite
syn match pbMessage2 "\<\w\+\>" nextgroup=pbMessageBody skipempty skipwhite
syn region pbMessageBody start=+{+ end=+}+ contains=@top,pbExtendKeyword fold contained


syn region  protoAnnotString  start=/"/ skip=/\\./ end=/"/ contained
syn region  protoAnnotString  start=/'/ skip=/\\./ end=/'/ contained
syn region pbAnnotation start=+\[+ end=+\]+ contains=protoAnnotString,pbAnnotationType,protoComment fold

syn keyword pbOptionKeyword option nextgroup=pbAnnotationType skipwhite
syn match pbAnnotationType +([a-z._]\+)+ nextgroup=annotationEquals contained skipwhite
syn match annotationEquals +=+ nextgroup=annotationValue contained skipwhite
syn match annotationValue +\w\++ contained

hi link annotationEquals Operator
hi link annotationValue Constant
hi link pbAnnotationType Identifier

hi link protoAnnotString String
hi link protoFieldType Type
hi link pbEnumTypeName Type
hi link pbMessage2 Type

hi link protoFieldName Identifier
hi link enumValue Constant

hi link pbAnnotation PreProc
hi! link pbOneofKeyword protoStructure
hi! link pbOneofName Identifier

hi! link pbEnumTypedef StorageClass
hi! link pbRepeat  StorageClass

hi! link pbOptionKeyword protoSyntax
hi! link pbMessageKeyword protoStructure
