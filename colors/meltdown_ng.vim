
if version > 580
  hi clear
  if exists("syntax_on")
    syntax reset
  endif
endif
let g:colors_name='meltdown_gen'

hi Magenta guifg=#ff83ff
hi DarkGrey guifg=#7b7b7b
hi LighterGrey guifg=#9a9a9a
hi Salmon guifg=#ff7779
hi Green1 guifg=#26ff00 ctermfg=106
hi Green3 guifg=#aba024 ctermfg=142
hi Green2 guifg=#3dab24 ctermfg=034
hi Sage1 guifg=#9f9a84 ctermfg=144
hi Sage3 guifg=#b0c4a3 ctermfg=144
hi Sage2 guifg=#a6b99a
hi Sage4 guifg=#8e9f84 ctermfg=108
hi Sage5 guifg=#8e9f84 ctermfg=108
hi Red3 guifg=#ff3448 ctermfg=203
hi Red2 guifg=#ff8f34 ctermfg=208
hi Red1 guifg=#ff5d34 ctermfg=202
hi Red6 guifg=#ff74f4 ctermfg=213
hi Red5 guifg=#ff9074 ctermfg=209
hi Red4 guifg=#ffa034 ctermfg=215
hi Fern1 guifg=#71bc78 ctermfg=071
hi Blue1 guifg=#80a0ff ctermfg=111
hi Blue3 guifg=#80ffdf ctermfg=122
hi Blue2 guifg=#af80ff ctermfg=141
hi Blue4 guifg=#c3bbff ctermfg=189
hi Teal1 guifg=#76dbd8 ctermfg=050
hi Teal2 guifg=#71fffa

" hi SignColumn guibg=#1a1818 ctermbg=234
hi Yellow1 guifg=#ffb700 ctermfg=179
hi Error guifg=#ff0000 ctermfg=009 guibg=#1a1818 ctermbg=234
hi Directory guifg=#80a0ff ctermfg=111 gui=bold cterm=bold
hi CursorLine guibg=#313030 ctermbg=236
hi NonText guifg=#ff5d34 ctermfg=111 guibg=#1f1d2a ctermbg=235 gui=bold cterm=bold
hi DarkTeal guifg=#346063 ctermfg=023
hi Orange5 guifg=#ffd780 ctermfg=222
hi Orange4 guifg=#ffb780 ctermfg=215
hi Orange6 guifg=#ff9780 ctermfg=209
hi Orange1 guifg=#ffb75f
hi Orange3 guifg=#ff552f ctermfg=009
hi Orange2 guifg=#ffbd2f ctermfg=214
hi Warning guifg=#ffff00 ctermfg=011 guibg=#1a1818 ctermbg=234
hi javaParen guifg=#80a0ff ctermfg=111 gui=bold cterm=bold
hi Sage3Mix guifg=#8ba3bb ctermfg=110
hi PMenu guifg=#ff7779 ctermfg=241 guibg=#14151d ctermbg=238
hi CursorColumn guibg=#313030 ctermbg=236
hi PMenuSel guifg=#ff5d34 ctermfg=202 guibg=#343030 ctermbg=236 gui=bold cterm=bold

hi TabLineFill guifg=#686060 ctermfg=241 guibg=#0f0e0e ctermbg=233
hi IncSearch guifg=#ff5d34 ctermfg=202 guibg=#000000 ctermbg=000 gui=reverse cterm=reverse
hi TabLineSel guifg=#1f1d2a guibg=#ff7779 ctermfg=202 ctermbg=233 gui=none
hi MatchParen guibg=#346063 ctermbg=023 gui=bold cterm=bold
hi VertSplit guifg=#0f0e0e ctermfg=233 guibg=#0f0e0e ctermbg=233
hi WinSeparator guifg=#0f0e0e ctermfg=233 guibg=#0f0e0e ctermbg=233
hi Normal guifg=#d8e5b8 ctermfg=187 guibg=#1f1d2a ctermbg=234
hi DarkGray guifg=#404040 ctermfg=238
hi StatusLine guifg=#ff5d34 ctermfg=202 guibg=#0f0e0e ctermbg=233 gui=bold cterm=bold
hi ColorColumn guibg=#4d5c8b ctermbg=060
hi Title guifg=#ff8f34 ctermfg=208 guibg=#444040 ctermbg=238 gui=bold cterm=bold
hi Visual guifg=#101010 ctermfg=233 guibg=#e0e0e0 ctermbg=188
hi CursorLineNr guifg=#ff5d34 ctermfg=202 guibg=#272424 ctermbg=235
hi StatusLineNC guifg=#686060 ctermfg=241 guibg=#0f0e0e ctermbg=233 gui=bold cterm=bold
hi Folded guifg=#000000 ctermfg=000 guibg=#9f9a84 ctermbg=144
hi TabLine guifg=#d4d4d4 guibg=#2d2d2d gui=none
hi Search guifg=#ffd780 ctermfg=222 guibg=#000000 ctermbg=000 gui=reverse cterm=reverse
hi SignColumn guibg=#1f1d2a ctermbg=234

hi! link TagbarAccessProtected Yellow1
hi! link javaDocTags Orange6
hi! link xmlTag Blue1
hi! link RadiationCppNamespace Sage2
hi! link RadiationJavaAbstractClass Red4
hi! link StorageClass Red1
hi! link Statement Blue1
hi! link RadiationJavaTypeParameter Red6
hi! link javaAnnotation PreProc
hi! link htmlEndTag Blue2
hi! link Type Red2
hi! link FoldColumn Green1
hi! link Function Yellow1
hi! link RadiationProtoType Red5
hi! link RadiationCppStruct Type
hi! link xmlAttrib Green3
hi! link RadiationJavaParam Orange3
hi! link RadiationJavaForIterator Sage3Mix
hi! link RadiationJavaMember Yellow1
hi! link Structure Red1
hi! link RadiationCppUnion Type
hi! link htmlTagName Blue1
hi! link PreProc Magenta
hi! link RadiationCppEnum Type
hi! link RadiationJavaLocalVar Sage2
hi! link TagbarAccessPublic Green1
hi! link xmlEndTag Red1
hi! link Identifier LighterGrey
hi! link Number Teal1
hi! link javaOperator Operator
hi! link RadiationJavaEnhancedForIterator RadiationJavaForIterator
hi! link String Green1
hi Comment guifg=#4b4b64 gui=italic
hi! link RadiationCppTypedef RadiationProtoType
hi! link javaCommentTitle Blue1
hi! link Constant PreProc
hi! link SpecialKey Blue1
hi! link javaDocComment Comment
hi! link TagbarAccessPrivate Red1
hi! link htmlTag Blue2
hi! link Operator Blue1
hi! link RadiationJavaInterface RadiationProtoType
hi! link Special Orange2

hi LineNrAbove guibg=#1f1d2a ctermfg=144 guifg=#5e5e5e ctermbg=234 gui=italic
hi LineNr guibg=#1f1d2a ctermfg=144 guifg=#5e5e5e ctermbg=234 gui=bold
hi! link LineNrBelow LineNrAbove
hi CursorLineNr guibg=#ff5d34 guifg=#2b293b gui=bold
hi DocComment guifg=#00d0ff gui=italic

hi! link @attribute PreProc
hi! link @comment.documentation.java DocComment
hi! link @constructor.lua @punctuation
hi! link @lsp.mod.classScope Salmon
hi! link @lsp.mod.functionScope Orange1
hi! link @lsp.mod.readonly PreProc
hi! link @lsp.type.class Type
hi! link @lsp.type.enum Type
hi! link @lsp.type.enum.java Red4
hi! link @lsp.type.interface.java Red5
hi! link @lsp.type.namespace @module
hi! link @lsp.type.operator Operator
hi! link @lsp.type.parameter Red1
hi! link @lsp.type.property Salmon
hi! link @lsp.type.field @lsp.type.property
hi! link @lsp.type.property.java @variable.member
hi! link @lsp.type.struct Type
hi! link @lsp.type.typeParameter Red6
hi! link @lsp.type.variable @variable
hi! link @lsp.typemod.method Function
hi! link @lsp.typemod.parameter.functionScope Red1
hi! link @lsp.typemod.property.static.java Sage3
hi! link @lsp.typemod.variable.functionScope.cpp LighterGrey
hi! link @module DarkGrey
hi! link @punctuation Blue1
hi! link @type.builtin.c Type
hi! link @type.qualifier Blue1
hi! link @variable Identifier
hi! link @variable.member Salmon
hi! link @variable.parameter Red1
hi! link TabLineFill SignColumn
hi! link luaTable @punctuation 
hi! link vimFunction Function
hi! link vimIsCommand Red1
hi! link vimUserFunc vimFunction

hi CmpItemAbbrDeprecated gui=strikethrough guifg=#ff7779 ctermfg=241 guibg=#14151d ctermbg=238
hi CmpItemAbbrMatch guifg=#1f1d2a guibg=#ff7779 gui=bold
hi PmenuSbar guibg=#333333
hi PmenuThumb guibg=#80a0ff

hi! link CmpDefault PMenu
hi! link CmpBorder CmpDefault
hi! link CmpCompletion Red1
hi! link CmpCompletionBorder CmpDefault
hi! link CmpCompletionExtension CmpDefault
hi! link CmpCompletionSbar CmpDefault
hi! link CmpCompletionSel CmpDefault
hi! link CmpCompletionThumb String
hi! link CmpDocumentation CmpDefault
hi! link CmpDocumentationBorder CmpDefault
hi! link CmpGhostText CmpDefault
hi! link CmpItemAbbr CmpDefault
hi! link CmpItemAbbrDefault CmpDefault
hi! link CmpItemAbbrDeprecatedDefault CmpItemAbbrDeprecated
hi! link CmpItemAbbrMatchDefault CmpDefault
hi! link PmenuSel CmpItemAbbrMatch
hi! link CmpItemAbbrMatchFuzzy CmpItemAbbrMatch
hi! link CmpItemAbbrMatchFuzzyDefault CmpItemAbbrMatch
hi! link CmpItemKind CmpDefault
hi! link CmpItemKindArray Constant
hi! link CmpItemKindBoolean Constant
hi! link CmpItemKindClass Type
hi! link CmpItemKindCodeium Constant
hi! link CmpItemKindColor Constant
hi! link CmpItemKindConstant Constant
hi! link CmpItemKindConstructor Constant
hi! link CmpItemKindCopilot Constant
hi! link CmpItemKindDefault Constant
hi! link CmpItemKindEnum Constant
hi! link CmpItemKindEnumMember Constant
hi! link CmpItemKindEvent Constant
hi! link CmpItemKindField Constant
hi! link CmpItemKindFile Constant
hi! link CmpItemKindFolder Constant
hi! link CmpItemKindFunction Function
hi! link CmpItemKindInterface Type
hi! link CmpItemKindKey Constant
hi! link CmpItemKindKeyword Constant
hi! link CmpItemKindMethod Constant
hi! link CmpItemKindModule Constant
hi! link CmpItemKindNamespace Constant
hi! link CmpItemKindNull Constant
hi! link CmpItemKindNumber Constant
hi! link CmpItemKindObject Constant
hi! link CmpItemKindOperator Constant
hi! link CmpItemKindPackage Constant
hi! link CmpItemKindProperty Constant
hi! link CmpItemKindReference Constant
hi! link CmpItemKindSnippet Constant
hi! link CmpItemKindString Constant
hi! link CmpItemKindStruct Constant
hi! link CmpItemKindTabNine Constant
hi! link CmpItemKindText Constant
hi! link CmpItemKindTypeParameter Identifier
hi! link CmpItemKindUnit Constant
hi! link CmpItemKindValue Constant
hi! link CmpItemKindVariable Identifier
hi! link CmpItemMenu DocComment
hi! link CmpItemMenuDefault DocComment
hi! link CmpKind CmpDefault
hi! link CmpReady CmpDefault
hi! link CmpStatus CmpDefault
hi! link CmpZsh CmpDefault
hi! link DiagnosticInfo Teal2
hi DiagnosticError guifg=red
hi ColorColumn guifg=#644141 guibg=none gui=None

hi! link vimAugroup PreProc
hi! link vimAutoCmdGroup vimAugroup
hi! link vimAutoCmdSfxList vimAugroup
hi! link vimAugroupEnd vimAugroup
hi! link vimUsrCmd vimCommand
hi! link vimSetEqual Number

hi TabLineCloseSel guifg=#790000 guibg=#ff5d34
hi TabLineClose guifg=#0f0e0e guibg=#2d2d2d
hi TabLineTail guifg=#ff8082 guibg=#0f0e0e

hi DiagnosticUnderlineError gui=undercurl
hi DiagnosticUnderlineWarn gui=undercurl
hi DiagnosticUnderlineInfo gui=undercurl

hi WarpNormal guibg=#ff7779 guifg=black gui=bold

hi! link TelescopeBorder Normal
hi TelescopeBorder blend=255
hi TelescopeNormal guibg=#12131b guifg=#ff7779
hi TelescopeSelection guibg=#ff7779 guifg=black
hi TelescopeTitle guifg=#ff7779 gui=bold
hi! link TelescopeMatching TelescopeNormal
hi TelescopeMatching gui=bold gui=reverse

hi! link vimMapRhs Red6
hi! link vimMapRhsExtend vimMapRhs
hi! link Delimiter Operator
hi! link vimMapLhs Typedef
