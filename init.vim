let g:loaded_netrw = 1
let g:loaded_netrwPlugin = 1

if filereadable('/etc/hostname')
  let hostname=join(readfile('/etc/hostname'))
else
  let hostname='unknown'
endif

let g:vsnip_snippet_dir = "~/.config/nvim/snippets"
let g:vsnip_snippet_dirs = ["~/.config/nvim/local/snippets"]

call plug#begin()

if filereadable(printf('%s/.config/nvim/local-plug.vim', $HOME))
  exec "source " . printf('%s/.config/nvim/local-plug.vim', $HOME)
endif

Plug 'nvim-lua/lsp-status.nvim'
Plug 'bfrg/vim-jq'
Plug 'folke/trouble.nvim'
Plug 'git@git.josher.dev:bulletjava.vim.git'
Plug 'git@git.josher.dev:fieldmarshal.vim.git'
Plug 'git@git.josher.dev:nvim-color-picker.git'
Plug 'git@git.josher.dev:nvim-warp.git'
Plug 'git@git.josher.dev:meltdown.git'
Plug 'google/vim-codefmt'
Plug 'google/vim-glaive'
Plug 'google/vim-maktaba'
Plug 'hrsh7th/cmp-buffer'
Plug 'hrsh7th/cmp-nvim-lsp'
Plug 'hrsh7th/cmp-nvim-lua'
Plug 'hrsh7th/cmp-path'
Plug 'hrsh7th/cmp-vsnip'
Plug 'hrsh7th/nvim-cmp'
Plug 'hrsh7th/vim-vsnip'
Plug 'kyazdani42/nvim-web-devicons'
" Plug 'lukas-reineke/virt-column.nvim'
Plug 'nanozuki/tabby.nvim'
Plug 'neovim/nvim-lspconfig'
Plug 'nvim-lua/plenary.nvim'
Plug 'nvim-lualine/lualine.nvim'
Plug 'nvim-telescope/telescope.nvim'
Plug 'nvim-tree/nvim-tree.lua'
Plug 'nvim-treesitter/nvim-treesitter', {'do': ':TSUpdate'}
Plug 'nvim-treesitter/nvim-treesitter-textobjects'
Plug 'onsails/lspkind.nvim'
" Plug 'tpope/vim-surround'
Plug 'kylechui/nvim-surround'
Plug 'vito-c/jq.vim'
Plug 'EdenEast/nightfox.nvim'
Plug 'sphamba/smear-cursor.nvim'

Plug 'lewis6991/gitsigns.nvim'

if has('rneovim')
  Plug 'git@git.josher.dev:rneovim-userregs.git'
endif
Plug 'git@git.josher.dev:config.vim.git',
      \ { 'dir': g:plug_home . '/config.vim',
      \   'do': ':UpdateInitVimHook' }
call plug#end()
command! UpdateInitVimHook source ~/.config/nvim/init.vim <bar> PlugInstall

set termguicolors
set shiftwidth=2
set tabstop=2
set expandtab
set nowrap
set splitright
set splitbelow
set wildmode=longest,list,full
set scrolloff=8
set number
set relativenumber
set pumheight=20
set updatetime=1000
set matchpairs+=<:>

let mapleader=" "

function! Jump(...)
  normal! `[
endfunction
noremap <silent> g. <cmd>set operatorfunc=Jump<cr>g@

" Make splitting more congruent with how Tmux does it.
noremap <C-w>% <C-w>v
noremap <C-w>" <C-w>s

" Open a terminal in a split below the current file.

noremap <C-w><C-t> <C-w>s<cmd>term<cr>
" Opens a terminal in the directory the current file is in.
noremap <C-w><C-S-t> <C-w>s<cmd>TERM<cr>

" Fix the yank behavior.
noremap Y y$

" Format code. This is <AltGr-u>
noremap ú <cmd>lua run_format_code()<cr>

" Synstack to help profile syntax highlighting issues.
noremap <leader>p <plug>(SynStack)

" Way to find files.
noremap <leader>ff <cmd>Telescope find_files<cr>
noremap <M-f> <cmd>Telescope find_files<cr>
noremap <M-r> <cmd>Telescope find_files<cr>
function! s:find_in_dir()
  exec "Telescope find_files search_dirs={\"" . escape(resolve(expand('%:h')), ' ') . "\"} path_display={\"shorten\"} hidden=true"
endfunction
noremap <M-F> <cmd>call <sid>find_in_dir()<cr>

inoremap <C-+> <Plug>(vsnip-expand)

" When navigating to the beginning of a line, I want to always reset the screen
" to the leftmost position.
noremap ^ ^zH

noremap <leader>t <cmd>TroubleToggle<cr>

augroup InitVim
  au!
  " Automatically stop highligting things when leaving insert mode. If I want
  " the highlight back, I can just hit 'n' and it will come back.
  "
  " I don't know why it can't just be 'noh' and why I have to do this feedkeys
  " nonsense, but there appears to be a bug of some kind.
  autocmd InsertLeave * call feedkeys("\<cmd>noh\<cr>")

  autocmd TextYankPost *
        \ lua require'vim.highlight'.on_yank(
        \   { higroup = "IncSearch", timeout = 100 });
  " autocmd CursorHold * lua vim.lsp.buf.document_highlight()
  " autocmd CursorHoldI * lua vim.lsp.buf.document_highlight()
  " autocmd CursorMoved * lua vim.lsp.buf.clear_references()
augroup END

" Local configuration that can be set by hostname or just a local configuration.
" Local files are ignored by .gitignore so they won't be added to the repo,
" allowing local (*cough* work) configuration to be separate from general
" configuration.

if isdirectory(printf('%s/.config/nvim/%s', $HOME, hostname))
  exec "set rtp+=" . printf('%s/.config/nvim/%s', $HOME, hostname)
endif

if isdirectory(printf('%s/.config/nvim/local', $HOME))
  exec "set rtp+=" . printf('%s/.config/nvim/local', $HOME)
endif

if filereadable(printf('%s/.config/nvim/%s.vim', $HOME, hostname))
  exec "source " . printf('%s/.config/nvim/%s.vim', $HOME, hostname)
endif

if filereadable(printf('%s/.config/nvim/local.vim', $HOME))
  exec "source " . printf('%s/.config/nvim/local.vim', $HOME)
endif

if isdirectory(printf('%s/.config/nvim/after', $HOME))
  exec "set rtp+=" . printf('%s/.config/nvim/after', $HOME)
endif

noremap <leader>nt <cmd>NvimTreeToggle<cr>
noremap <C-n><C-e> :<C-u>e <C-r>=escape(expand('%:h'), ' \')<cr>/
noremap <C-w>. <cmd>lua require('windownav').goto_terminal()<cr>
noremap <C-w>I <cmd>lua require('windownav').goto_initvim()<cr>
noremap <C-w>' <cmd>lua require('windownav').goto_lastwin()<cr>

nnoremap <M-t> <cmd>term<cr><cmd>startinsert<cr>
nnoremap <M-S-T> <cmd>TERM<cr><cmd>startinsert<cr>

nnoremap <C-Space> <C-^>
nnoremap <M-b> <cmd>Telescope buffers<cr>
nnoremap <M-l> <cmd>bnext<cr>
nnoremap <M-h> <cmd>bprev<cr>

noremap <C-g> <cmd>lua vim.diagnostic.open_float()<cr>

noremap <leader><C-f> <cmd>WarpFull<cr>

noremap <C-f> <cmd>WarpGrid<cr>
onoremap <C-f> v<cmd>WarpGrid<cr>

noremap <leader>w <cmd>WarpWords<cr>
onoremap <leader>w v<cmd>WarpWords<cr>

" <C-/> - search within last visual selection.
noremap  /<C-u>\%V
nmap œ <leader>k
nmap ï <leader>j
omap œ <leader>k
omap ï <leader>j
omap aœ a<leader>k
omap aï a<leader>j
omap iœ i<leader>k
omap iï i<leader>j
vmap œ <leader>k
vmap ï <leader>j

noremap <M-ñ> <cmd>TSNodeUnderCursor<cr>
noremap <M-©> <cmd>TSCaptureUnderCursor<cr>

" Greek capital tau (τ)
noremap Τ <cmd>Inspect<cr>

command! Config edit $HOME/.config/nvim/init.vim
command! Configl edit $HOME/.config/nvim/local.vim

cnoreabbrev <expr> _config expand("$HOME/.config/nvim")
cnoreabbrev <expr> _plugged expand("$HOME/.local/share/nvim/plugged")
cnoreabbrev <expr> _fm expand("$HOME/.local/share/nvim/plugged/fieldmarshal.vim")
cnoreabbrev <expr> _home expand("$HOME/.local/share/nvim")
cnoreabbrev <expr> _projects expand("$HOME/Projects")
cnoreabbrev <expr> _ escape(expand('%:h'), ' \')

noremap g[r <cmd>Telescope lsp_references path_display={"shorten"}<cr>
noremap g[d <cmd>Telescope lsp_definitions path_display={"shorten"}<cr>
noremap g[i <cmd>Telescope lsp_implementations path_display={"shorten"}<cr>
noremap g[td <cmd>Telescope lsp_type_definitions path_display={"shorten"}<cr>
noremap g[ci <cmd>Telescope lsp_incoming_calls path_display={"shorten"}<cr>
noremap g[co <cmd>Telescope lsp_outgoing_calls path_display={"shorten"}<cr>
noremap g[sd <cmd>Telescope lsp_document_symbols path_display={"shorten"}<cr>
noremap g[sw <cmd>Telescope lsp_workspace_symbols path_display={"shorten"}<cr>
noremap g[sdw <cmd>Telescope lsp_dynamic_workspace_symbols path_display={"shorten"}<cr>

set textwidth=80
set colorcolumn=+1
if has('rneovim')
  " call HighlightColorColumns()
  " " Grey color column right after text width is grey and in the background and
  " " a red one after the text width which is red and in the foreground.
  " set colorcolumn=+1/│/MyColorColumn/b,+20/│/MyRedColorColumn/f
endif

" Opens a terminal in the directory of the current file.
command! TERM exec "term sh -c " . shellescape(printf("cd %s && exec ", shellescape(expand('%:p:h')))) . '$SHELL'

" W = w. I often click when typing :w
command! W w

let g:loaded_netrw = 1
let g:loaded_netrwPlugin = 1

augroup UseTerminal
  au!
  au BufNewFile * lua open_terminal_on_directory()
  au BufEnter * lua open_terminal_on_directory()
augroup END


let g:jq_highlight_objects = 1
let g:jq_highlight_function_calls = 1

" Increment the character under the cursor to the next character in order.
function! OrdNext()
  let char = matchstr(getline('.'), '\%' . col('.') . 'c.')
  let nr = char2nr(char)
  let new_char = nr2char(nr + 1)
  exec "norm r" . new_char
endfunction

" Decrement the character under the cursor to the last character in order.
function! OrdPrev()
  let char = matchstr(getline('.'), '\%' . col('.') . 'c.')
  let nr = char2nr(char)
  let new_char = nr2char(nr - 1)
  exec "norm r" . new_char
endfunction

noremap α <cmd>call OrdNext()<cr>
" Capital alpha (α)
noremap Α <cmd>call OrdPrev()<cr>

noremap € vib<esc>a)<esc>

noremap <X2Mouse> <C-I>
noremap <2-X2Mouse> <C-I>
noremap <3-X2Mouse> <C-I>

noremap <X1Mouse> <C-O>
noremap <2-X1Mouse> <C-O>
noremap <3-X1Mouse> <C-O>

noremap <M-S-ScrollWheelRight> <C-LeftMouse>

menu 0.400 PopUp.Find\ References <Cmd>norm g[r<CR>
menu 0.400 PopUp.Find\ Implementers <Cmd>norm g[i<CR>
menu PopUp.Split :split<cr>
menu PopUp.VSplit :vsplit<cr>
menu PopUp.Close :q<cr>

lua << EOF
  function open_terminal_on_directory()
    local buf = vim.api.nvim_get_current_buf()
    local bufname = vim.api.nvim_buf_get_name(buf)

    if vim.fn.isdirectory(bufname) ~= 1 then
      return
    end

    local tcmd = string.format("cd %s && exec $SHELL", vim.fn.shellescape(bufname))
    local cmd = string.format("term sh -c %s", vim.fn.shellescape(tcmd))


    vim.cmd(cmd)
    vim.cmd("startinsert")
  end

  require'nvim-treesitter.configs'.setup {
    textobjects = {
      select = {
        enable = true,

        -- Automatically jump forward to textobj, similar to targets.vim
        lookahead = true,

        keymaps = {
          -- You can use the capture groups defined in textobjects.scm
          ["am"] = "@function.outer",
          ["im"] = "@function.inner",
          ["af"] = "@function.outer",
          ["if"] = "@function.inner",
          ["aC"] = "@class.outer",
          ["aa"] = "@parameter.outer",
          ["ia"] = "@parameter.inner",
          ["iq"] = "@block.inner",
          ["aq"] = "@block.outer",
          ["aS"] = "@statement.outer",
          ["iS"] = "@statement.inner",
          -- You can optionally set descriptions to the mappings (used in the desc parameter of
          -- nvim_buf_set_keymap) which plugins like which-key display
          ["iC"] = { query = "@class.inner", desc = "Select inner part of a class region" },
        },
        -- You can choose the select mode (default is charwise 'v')
        --
        -- Can also be a function which gets passed a table with the keys
        -- * query_string: eg '@function.inner'
        -- * method: eg 'v' or 'o'
        -- and should return the mode ('v', 'V', or '<c-v>') or a table
        -- mapping query_strings to modes.
        selection_modes = {
          ['@parameter.outer'] = 'v', -- charwise
          ['@function.outer'] = 'V', -- linewise
          ['@function.inner'] = 'V', -- linewise
          ['@block.inner'] = 'V', -- linewise
          ['@block.outer'] = 'V', -- linewise
        },
        -- If you set this to `true` (default is `false`) then any textobject is
        -- extended to include preceding or succeeding whitespace. Succeeding
        -- whitespace has priority in order to act similarly to eg the built-in
        -- `ap`.
        --
        -- Can also be a function which gets passed a table with the keys
        -- * query_string: eg '@function.inner'
        -- * selection_mode: eg 'v'
        -- and should return true of false
        include_surrounding_whitespace = function (q)
          return string.match(q.query_string, '.inner$') == nil and
                    string.match(q.query_string, '^function')
        end,
      },
      move = {
        enable = true,
        set_jumps = true,
        goto_next_start = {
          ["]C"] = "@injectable_constructor.params.inner",
          ["]c"] = "@class.outer",
        },
        goto_previous_start = {
          ["[C"] = "@injectable_constructor.params.inner",
          ["[c"] = "@class.outer",
        },
      },
    },
  }

  require('nvim-treesitter.configs').setup({
    highlight = {
      enable = true,  -- Enables highlighting for all supported filetypes
      additional_vim_regex_highlighting = false,  -- Disable regex-based highlighting for speed
    },
  })

  function remove_package(str)
    for k, v in pairs(package.loaded) do
      if string.match(k, "^" .. str) then
        package.loaded[k] = nil
      end
    end
  end

  function reload_package(str)
    remove_package(str)
    return require(str)
  end

  -- CiderLSP
  vim.opt.completeopt = { "menu", "menuone", "noselect" }
  require("lsp")
  require("lspconfig")
  require("tabby")

  -- Diagnostics
  require("diagnostics")
  require("nvim-tree").setup({
    hijack_directories = {auto_open = false}
  })

  require("lualinesetup")

  require('telescope').setup({
    defaults = {
      title = "",
      borderchars = { " ", " ", " ", " ", " ", " ", " ", " " },
      prompt_prefix = '🞂 ',
      selection_caret = '🞂 ',
      layout_strategy = "horizontal",
      results_title = false,
      sorting_strategy = "ascending",
      pickers = {
        find_files = {
          hidden = true
        }
      },
      -- path_display = "shorten",
      layout_config = {
          horizontal = {
              width = 0.66,
              height = 0.66,
              },
          -- other layout configuration here
          },
      -- other defaults configuration here
      },
    -- other configuration values here
    })

  -- require("virt-column").setup({
  --   char = '│',
  --   highlight = "ColorColumnLine"
  -- })

  function run_format_code()
    local lsps = vim.lsp.buf_get_clients()
    if lsps and #lsps > 0 then
      vim.lsp.buf.format()
    else
      vim.cmd("FormatCode")
    end
  end

  local gitsigns = require("gitsigns")
  gitsigns.setup({
    on_attach = function(bufnr)
      vim.keymap.set({'o', 'x'}, 'ih', '<Cmd>Gitsigns select_hunk<CR>')
        
      local function map(mode, l, r, opts)
        opts = opts or {}
        opts.buffer = bufnr
        vim.keymap.set(mode, l, r, opts)
      end

      -- Navigation
      map('n', ']h', function()
        if vim.wo.diff then
          vim.cmd.normal({']c', bang = true})
        else
          gitsigns.nav_hunk('next')
        end
      end)

      map('n', '[h', function()
        if vim.wo.diff then
          vim.cmd.normal({'[c', bang = true})
        else
          gitsigns.nav_hunk('prev')
        end
      end)

      -- Actions
      map('n', '<leader>hs', gitsigns.stage_hunk)
      map('n', '<leader>hr', gitsigns.reset_hunk)

      map('v', '<leader>hs', function()
        gitsigns.stage_hunk({ vim.fn.line('.'), vim.fn.line('v') })
      end)

      map('v', '<leader>hr', function()
        gitsigns.reset_hunk({ vim.fn.line('.'), vim.fn.line('v') })
      end)

      map('n', '<leader>hS', gitsigns.stage_buffer)
      map('n', '<leader>hR', gitsigns.reset_buffer)
      map('n', '<leader>hp', gitsigns.preview_hunk)
      map('n', '<leader>hi', gitsigns.preview_hunk_inline)
    end
  })

  require("nvim-surround").setup({})
  require('smear_cursor').enabled = true
  require('smear_cursor').cursor_color = '#ff0000'
  require('smear_cursor').legacy_computing_symbols_support = true
  require('smear_cursor').stiffness = 0.4
  require('smear_cursor').distance_stop_animating = 5
EOF

